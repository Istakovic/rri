using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextControler : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] Text storyText;
    private enum States{
        sleep,get_up, heat_watter, pick_up, eventualy_get_up,play_games,victory, go_to_WC,sleep_forever
    };
    private States myState;
    void Start()
    {
        storyText.text = "Our first start text";
        myState  = States.sleep;
    }

    // Update is called once per frame
    void Update()
    {
        if(myState==States.sleep){
            sleep();
        }else if(myState==States.get_up){
            get_up();
        }else if(myState==States.pick_up){
            pick_up();
        }else if(myState==States.heat_watter){
            heat_watter();
        }else if(myState==States.sleep_forever){
            sleep_forever();
        }else if(myState==States.eventualy_get_up){
            eventualy_get_up();
        }else if(myState==States.play_games){
            play_games();
        }else if(myState==States.go_to_WC){
            go_to_WC();
        }
        
    }
    void sleep(){
        storyText.text = "Getting up early is really disturbing and noone loves it.\n You just woke up and you dont know where you are because it is to early in the morning.\n \n Your mind is telling you to get up, but your body doesnt listen.What will you do?"+
        "\n Press A to turn of the allarm and go back to sleep."+
        "\n Press B to get up.";

        if(Input.GetKeyDown(KeyCode.A)){
            myState = States.eventualy_get_up;
        }else if(Input.GetKeyDown(KeyCode.B)){
            myState = States.get_up;
        }
        }

        void get_up(){
        storyText.text = "Got out of bed, go to kitchen make some coffee.\n\n"+
        "\n You idiot,you spilled the water while making coffee,press C to pick it up!"+
        "\n Press D to successfully put the water to heat up.";

        if(Input.GetKeyDown(KeyCode.C)){
            myState = States.pick_up;
        }else if(Input.GetKeyDown(KeyCode.D)){
            myState = States.heat_watter;
        }
        }

        void pick_up(){
        storyText.text = "While the water is on the flour:\n\n"+
        "\n Press Q to pick it up and putt the water back again to heat up"+
        "\n Press W to do nothing and go to sleep";

        if(Input.GetKeyDown(KeyCode.Q)){
            myState = States.heat_watter;
        }else if(Input.GetKeyDown(KeyCode.W)){
            myState = States.sleep;
        }
        }

        void heat_watter(){
        storyText.text = "Drink coffe!\n\n"+
        "\n Press P to make another one"+
        "\n After drinking coffee, you decide you are still sleepy, Press W to go to sleep again ";

        if(Input.GetKeyDown(KeyCode.P)){
            myState = States.get_up;
        }else if(Input.GetKeyDown(KeyCode.W)){
            myState = States.sleep;
        }
        }

        void sleep_forever(){
        storyText.text = "You will sleep forever and dont care about obligations!\n\n"+
        "\n Press P to try to wake up!"+
        "\n YOu are dead, no hope for you. press T to vixtory ;)";

        if(Input.GetKeyDown(KeyCode.P)){
            myState = States.get_up;
        }else if(Input.GetKeyDown(KeyCode.T)){
            myState = States.victory;
        }
        }
        void eventualy_get_up(){
        storyText.text = "Notices you can not sleep because of stupid collage\n\n"+
        "\n Press P even due to collage to continue sleeping"+
        "\n Press O to get up!)";

        if(Input.GetKeyDown(KeyCode.P)){
            myState = States.play_games;
        }else if(Input.GetKeyDown(KeyCode.O)){
            myState = States.get_up;
        }
        }
        void play_games(){
        storyText.text = "You woke up, and decided to play games, you won the game!\n\n"+
        "\n Press P to make coffee!"+
        "\n Press O go to WC!)";

        if(Input.GetKeyDown(KeyCode.P)){
            myState = States.get_up;
        }else if(Input.GetKeyDown(KeyCode.O)){
            myState = States.go_to_WC;
        }
        }

        void go_to_WC(){
        storyText.text = "After WC:\n\n"+
        "\n Press P to go to sleep!"+
        "\n Press O to go play more video games)";

        if(Input.GetKeyDown(KeyCode.P)){
            myState = States.sleep;
        }else if(Input.GetKeyDown(KeyCode.O)){
            myState = States.play_games;
        }
        }

}
